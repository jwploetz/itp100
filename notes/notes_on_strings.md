1.  What does Dr. Chuck say about indexing strings? How does this operation work? Provide a few examples.  
Indexing a string is when you tell what position a character is at in a string for example the e in hello is `hello[1]`  

2.  Which Python function returns the number or characters in a string? Provide a few examples of it being used.  
`len` returns the legnth of a string, for example `len(hello)` would return 5.  

3.  Discuss the two ways Dr. Chuck shows to loop over the characters in a string. Provide an example of each.  
The first way that he loops over it is with a while function, for example `while index < len(str)`: for example. The second way is a for loop, for example, `for letter in fruit: print(letter)`  

4.  What is slicing? Explain it in your own words and provide several examples of slicing at work. including examples using default values for start and end.  

Slicing is a way of taking only part of a string, for example if `hello = "hello" `hello[0:2]` would return "he", and `hello[3:5]` would return "ll"  

5.  What is concatenation as it applies to strings. Show an example.  
concatenation is a method of combing strings, for example `"hello, " + "world"` would return "hello, world"  

6.  Dr. Chuck tells us that in can be used as a logical operator. Explain what this means and provide a few examples of its use this way with strings.  
`in` is a way to se if one string is in another string, for example `"he" in "hello"` would return `True`  

7.  What happens when you use comparison operators (>, <, >=, <=, ==, !=) with strings?  
The `>, <, >=, and <=` comparison operators all compare the lengths of strings they same as they would with numbers. `==` compares if the contents of a string are the same, and `!=` is the inverse of `==`  

8.  What is a method? Which string methods does Dr. Chuck show us? Provide several examples.  
Methods are functions that are specific to a class. For example if `str = "hello"`, `str.upper()` would return HELLO  

9.  Define white space.  
whitespace are spaces or non-printing characters  

10. What is Unicode?  
Unicode is the standard for encoding, representation, and handling or characters.
