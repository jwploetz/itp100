1.  Describe the characteristics of a collection.  

A collection is a single variable able to hold multiple values.  

2.  Using the video for inspiration, make a high level comparison of lists and dictionaries. How should you think about each of them?  

lists are a collection that is in a certain order, dictionaries however are a collection of values in no certain order, but instead defined by labels. Lists are like a tent bag before you open it for the first time, nice and organized, and a dictionary is the tent bag after you put everything back in, you know what everything is, but its in a random order.  

3.  What is the synonym Dr. Chuck tells us for dictionaries, that he presents in the slide showing a similar feature in other programming languages?  

he tells us dictionaries are like bags, because everything in there is not ordered.  

4.  Show a few examples of dictionary literals being assigned to variables.  

`jjj = { 'chuck': 1 , 'fred': 42, 'jan': 100 }`

5.  Describe the application in the second video which Dr. Chuck says is one of the most common uses of dictionaries.  

He says that dictionaries are useful for counting things, especially useful for making histograms.  

6.  Write down the code Dr. Chuck presents run this application.  

`counts = dict() names = ['csev', 'cwen', csev', 'zqian', 'cwen'] for name in names: if name not in counts: counts[name] = 1 else : counts[name] = counts[name] + 1 print(counts)`  

7.  What is the dictionary method that makes this common operation shorter (from 4 lines to 1)? Describe how to use it.

`dictionary.get(key, number)` checks if key exists, if it does its value is returned, if not it is sat to a value which is usually 0.

8.  Describe the application that Dr. Chuck leads into in the third video and codes in front of us in the fourth video.  

it takes a text file and prints out the most common word.
