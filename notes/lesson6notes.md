1.  Dr. Chuck calls looping "the 4th basic pattern of computer programming." What are the other three patterns?  
Conditional, sequential, and store and reuse.  

2.  Describe the program Chr. Chuck uses to introduce the while loop. What does it do?  
It counts down from 5 then prints blastoff  

3.  What is another word for looping introduced in the video?  
He uses the term "iteration"  

4.  Dr. Chuck shows us a broken example of a loop. What is this kind of loop called? What is wrong with it?  
Its called and infinite loop, what's wrong with it is that the value is not decreasing.  

5.  What Python statement (a new keyword for us, by the way) can be used to exit a loop?  
The keyword is `break`  

6.  What is the other loop control statement introduced in the first video? What does it do?  
`continue` moves the loop to the next iteration  

7.  Dr. Chuck ends the first video by saying that a while is what type of loop? Why are they called that? What is the type of loop he says will be discussed next?  
while loops are indefinite loops, the next loop is definite loops  

8.  Which new Python loop is introduced in the second video? How does it work?  
a for loop, it works by iterating a set number of times  

9.  The third video introduces what Dr. Chuck calls loop idioms. What does he mean by that term? Which examples of loop idioms does he introduce in this and the fourth video?  
loop idioms are the patterns in how we construct loops, some examples are finding the largest, smallest, or average value.
