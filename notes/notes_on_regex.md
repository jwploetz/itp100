1.  What are regular expressions? What are they used for? When did they originate?  

Regular expressions are wild card expressions used for parsing and matching strings, they are essentially a "smart search." They originated in the 60s.  

2.  Use Markdown to reproduce the Regular Expression Quick Guide slide.  

^ Matches the beginning of a line  
$ Matches the end of the line  
. Matches any character  
\\s Matches whitespace  
\\S Matches any non-whitespace character  
\* Repeats a character zero or more times  
\*? Repeats a character zero or more times (non-greedy)  
\+ Repeats a character one or more times  
\+? Repeats a character one or more times (non-greedy)  
[aeiou] Matches a single character in the listed set  
[^XYZ] Matches a single character not in the listed set  
[a-z0-9] The set of character can include a range  
( Indicates where string extraction is to start  
) Indicates where string extraction is to end  
