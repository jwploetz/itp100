1.  How can we use Markdown and git to create nice looking documents under revision control?  
Markdown is used to easily create nice looking text files, it is generally used to make repo READMEs and installation instructions, but it can be used for other things, for example taking notes.  

2.  Dr. Chuck describes functions as store and reused pattern in programming. Explain what he means by this. (Note: I will present another view of functions, as a tool for allowing procedural abstraction, in our class discussion, and will emphasize just what an important idea that is).  
functions are a set of instructions that can be written once, then called later to complete a task, overall reducing the complexity of a program.  

3.  How do we create functions in Python? What new keyword do we use? Provide your own example of a function written in Python.  
Functions are created with the code `def "funtion_name"(var1, var2, var3):`, any instructions in the function must be indented. The following function adds two numbers together and return the output:  
```
def add(num1, num2:
    sum = num1 + num2
    return sum
```  
4.  Dr. Chuck shows that nothing is output when you define a function, what he calls the store phase. What does he call the process that makes the function run? He uses two words for this, and it is really important to understand this idea and learn the words for it.  
calling a function runs the set of instructions in a function, it is important to know this because it is necessary to know how to use function is programming.  

5.  Provide some examples of built-in function in Python.  
some examples are `float()`, `input()`, `int()`, and `type()`
