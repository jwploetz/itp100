1.  Dr. Chuck begins this lesson by stating that "tuples are really a lot like lists. In what ways are tuples like lists? Do you agree with his statement that they are a lot alike?"  

Both have a collection of items that can be called in functions, for these reasons I agree with his statement.  

2.  How do tuples essentially differ from lists? Why do you think there is a need for this additional data type if it is similar to a list?  

The differ because unlike lists, they are immutable. They are probably needed because they are more efficient.  

3.  Dr. Chuck says that tuple assignment is really cool (Your instructor completely agrees with him, btw). Describe what tuple assignment is, showing a few examples of it in use. Do you think it is really cool? Why or why not?  


With tuple assignment you can collapse multiple assignment lines into on line. For example in `(x,y) = (25, 50)`, `print(y)` would return `50`. I think it's cool because it allows you to condense your code and make it easier to read.  

4.  Summarize what you learned from the second video in this lesson. Provide example code to make support what you describe.  

Video 2 is about sorting tuples. The function `sorted()` sorts tuples.  

5.  In the slide titled Even Shorter Version Dr. Chuck introduces list comprehensions. This is another really cool feature of Python. Did the example make sense to you? Do you think you understand what is going on?  

Yes the list comprehension example made sense and I understand what's going on, I believe I used this last year
