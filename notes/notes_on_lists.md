1.  In the first video of this lesson, Dr. Chuck discusses two very important concepts: algorithms and data structures. How does he define these two concepts? Which one does he say we have been focusing on until now?  

Algorithms are sets of instructions that computers follow, and data structures are a way of organizing data in a computer  

2.  Describe in your own words what a collection is.  

A list that has multiple values in it.  

3.  Dr. Chuck makes a very important point in the slide labeled Lists and definite loops - best pals about Python and variable names that he has made before, but which bares repeating. What is that point?  

His point is that python is very literal and does exactly what is told, so while a human may think of the variable names `friend` and `friends` as the same thing, python interprets them as separate variables.  

4.  How do we access individual items in a list?  

To access items from lists you use brackets `[]`,to access the first item you do `list[0]`, the second item you do `list[1]`, and so on.  

5.  What does mutable mean? Provide examples of mutable and immutable data types in Python.  

Mutable means something can be changed, lists are mutable, and stings are immutable.  

6.  Describe several list operations presented in the lecture.  

concatenation is essentially adding of lists, the in operator checks if something is in a list then returns a boolean value, and slicing is taking away parts of lists.  

7.  Describe several list methods presented in the lecture.  

`sort` will sort the list items alphabetically, or numerically if the values are numbers. `pop` will remove a certain item from a list.  

8.  Describe several useful functions that take lists as arguments presented in the lecture.  

`max` will return the largest value in a list, `min` will return the smallest value in a list, and `len` will return the number of things in a list.  

9.  The third video describes several methods that allow lists and strings to interoperate in very useful ways. Describe these.  

The `split` method allow you to convert a string into a list, whitespace in the string will act as breaks and the string will be split up there.  

10. What is a guardian pattern? Use at least one specific example in describing this important concept.  

A guardian pattern is a statement placed before a statement that can return an error.
