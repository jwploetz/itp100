import string

lower = string.ascii_lowercase
upper = string.ascii_uppercase
original = open("sample2.txt")
output = ""
key = int(original.readline().rstrip())

for line in original:
    if line.rstrip() == "STOP":
        break

    else:
        for letter in line:
            if letter.lower() in lower:
                alphabet = lower if letter in lower else upper
                index = alphabet.index(letter)
                output += alphabet[(index + key) % 26]
            else:
                output += letter

print(output)
