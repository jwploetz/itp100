data = open("gene_splicing_test.dat")
next(data)

for line in data:
    iterations = 0
    split_line = line.split()

    #defines variables
    a = float(split_line[0])
    b = float(split_line[1])
    c = float(split_line[2])
    t = float(split_line[3])
    target = a/b

    dish_1 = [a, 0.0]
    dish_2 = [0.0, b]

    while True:
        total_1 = dish_1[0] + dish_1[1]

    #mixes c mL from dish_1 into dish_2
    dish_2 = [dish_2[0] + c * (dish_1[0] / total_1), dish_2[1] + c * (dish_1[1] / total_1)]
    dish_1 = [dish_1[0] - c * (dish_1[0] / total_1), dish_1[1] - c * (dish_1[1] / total_1)]

    total_2 = dish_2[0] + dish_2[1]

    #mixes c mL from dish_2 into dish_1
    dish_1 = [dish_1[0] + c * (dish_2[0] / total_2), dish_1[1] + c * (dish_2[1] / total_2)]
    dish_2 = [dish_2[0] - c * (dish_2[0] / total_2), dish_2[1] - c * (dish_2[1] / total_2)]

    iterations += 1

    mix_1 = dish_1[0] / dish_1[1]
    mix_2 = dish_2[0] / dish_2[1]

    #checks if mix is within tolerance
    if mix_1 != 0 and mix_2 != 0:
        if abs(mix_1 - target) <= t and abs(mix_2 - target) <= t:
            print(iterations)
            break
