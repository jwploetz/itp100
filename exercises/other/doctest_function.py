def smallest(num1, num2, num3):
    """
    >>> smallest(3, 8, 5)
    3
    >>> smallest(42, 9, 11)
    9
    >>> smallest(42, 7, 91)
    7
    >>> smallest(5, 5, 5)
    5
    """
    nums = [num1, num2, num3]
    nums.sort()
    return nums[0]


if __name__ == '__main__':
    import doctest
    
    doctest.testmod()
