def count_sub(sub, s):
    """
      >>> count_sub('is', 'Mississippi')
      2
      >>> count_sub('an', 'banana')
      2
      >>> count_sub('ana', 'banana')
      2
      >>> count_sub('nana', 'banana')
      1
      >>> count_sub('nanan', 'banana')
      0
    """
    count = start = 0
    while True:
        start = s.find(sub, start) + 1
        if start > 0:
            count += 1
        else:
            return count

if __name__ == '__main__':
    import doctest
    doctest.testmod()
